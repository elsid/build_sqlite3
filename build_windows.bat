set SRC=%CD%
mkdir build
cd build

@call vcvars64.bat

cmake ^
    -D CMAKE_INSTALL_PREFIX=install\%1 ^
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=ON ^
    -D CMAKE_C_FLAGS="/Zi" ^
    %SRC%

cmake --build . --config %1
cmake --install . --config %1
