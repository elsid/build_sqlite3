#!/bin/bash -ex

SRC="${PWD}"
mkdir -p build/${1:?}
cd build/${1:?}
cmake \
    -D CMAKE_BUILD_TYPE=${1:?} \
    -D CMAKE_INSTALL_PREFIX=install \
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=ON \
    -D CMAKE_TOOLCHAIN_FILE=/usr/lib/android-sdk/ndk-bundle/build/cmake/android.toolchain.cmake \
    -D ANDROID_ABI=arm64-v8a \
    -D ANDROID_PLATFORM=android-21 \
    "${SRC}"
cmake --build .
cmake --install .
